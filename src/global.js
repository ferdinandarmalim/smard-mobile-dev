import React from 'react';
import { Dimensions } from 'react-native';

module.exports = {
    deviceWidth: Dimensions.get('window').width
}