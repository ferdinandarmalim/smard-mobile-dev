import React from 'react';
import { Icon} from '@shoutem/ui';
import { createBottomTabNavigator } from 'react-navigation';

//Stack Navigator
import HomeStack from './pages/home/HomeStack';
import FundsStack from './pages/funds/FundsStack';
import AuthStack from './pages/auth/AuthStack';
import OrderStack from './pages/order/OrderStack';

const TabNavigatorBeforeLogin = createBottomTabNavigator(
    {
        Home: {
            screen: HomeStack
        },
        'Mutual Funds': {
            screen: FundsStack
        },
        News:{
            screen: OrderStack
        },
        Login: {
            screen: AuthStack
        }
    },
    {
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state
                let iconName;
                if (routeName === 'Home')
                    iconName = "home"
                else if (routeName === 'Mutual Funds')
                    iconName = "equalizer"
                else if (routeName === 'Leaderboard')
                    iconName = "loyalty-card"
                else if (routeName === 'News')
                    iconName = "page"
                else if (routeName === 'Login')
                    iconName = "user-profile";

                return <Icon name={iconName} style={{ color: tintColor }} />
            }
        }),
        tabBarOptions: {
            activeTintColor: '#E58746',
            inactiveTintColor: '#959595'
        },
        initialRouteName: 'Home',
    }
);

export default TabNavigatorBeforeLogin;