export const SIGN_IN = 'SIGN_IN';
export const SIGN_OUT = 'SIGN_OUT';

//sign in
export function login() {
    return {
        type: SIGN_IN
    }
}

//sign out
export function logout() {
    return {
        type: SIGN_OUT
    }
}