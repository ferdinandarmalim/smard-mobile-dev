import gql from 'graphql-tag';

export const SettingsQuery = gql`
    query settings {
        settings {
            id
            logo
            favicon
            site_title
            site_status
            meta_title
            meta_description
            order_expire
            transaction_price
            transaction_point
        }
    }
`

export const CategoriesQuery = gql`
    query getCategoryList{
      getCategoryList{
        name
        slug
        childs{
          name
          slug
        }
      }
    }
`

export const ProductsQuery = gql`
    query products{
        products{
          id
          name
          description
          weight{
            unit
            weight
          }
          stock
          sku
          updated_at
          created_at
          image{
            filename
            size
          }
          price{
            min
            max
          }
          variant{
            key
            image
            price
            sku
            varStock
          }
          categories{
            name
            slug
          }
        }
      }
`

export const CartQuery = gql`
    query getCart($id: String!) {
      getCart(id: $id) {
        id
        IP
        total_price
        created_at
        updated_at
        status
        type
        member {
          id
          name
        }
        products {
          productID
          productName
          variant
          sku
          quantity
          price
          totalPrice
          detailProduct {
            id
            name
            description
            weight {
              unit
              weight
            }
            stock
            sku
            updated_at
            created_at
            image {
              filename
              size
            }
            price {
              min
              max
            }
            variant {
              key
              image
              price
              sku
              varStock
            }
            categories {
              name
              slug
            }
          }
        }
      }
    }
`

export const InquiryQuery = gql`
    query getInquiry($id: String!) {
      getInquiry(id: $id) {
        id
        member {
          id
          name
          email
        }
        inquiries {
          id
          productImage
          productName
          description
          quantity
          price
          totalPrice
          status{
            statusType
            statusNote
          }
        }
        cart_id
        total_price_cart
        status
        IP
      }
    }
`

export const OrdersQuery = gql`
    query getOrders($id: String!) {
      getOrders(id: $id) {
        id
        member {
          id
          name
          email
        }
        cart {
          id
          IP
          total_price
          created_at
          updated_at
          status
          type
          member {
            id
            name
          }
          products {
            productID
            productName
            variant
            sku
            quantity
            price
            totalPrice
            detailProduct {
              id
              name
              description
              weight {
                unit
                weight
              }
              stock
              sku
              updated_at
              created_at
              image {
                filename
                size
              }
              price {
                min
                max
              }
              variant {
                key
                image
                price
                sku
                varStock
              }
              categories {
                name
                slug
              }
            }
          }
        }
        inquiry{
          id
          member {
            id
            name
            email
          }
          inquiries {
            id
            productImage
            productName
            description
            quantity
            price
            totalPrice
            status{
              statusType
              statusNote
            }
          }
          cart_id
          total_price_cart
          status
          IP
        }
        status
        total_price
        type
        created_at        
      }
    }
`

export const OrdersMemberQuery = gql`
    query getOrdersByMember($id: String!) {
      getOrdersByMember(id: $id) {
        id
        member {
          id
          name
          email
        }
        cart {
          id
          IP
          total_price
          created_at
          updated_at
          status
          type
          member {
            id
            name
          }
          products {
            productID
            productName
            variant
            sku
            quantity
            price
            totalPrice
            detailProduct {
              id
              name
              description
              weight {
                unit
                weight
              }
              stock
              sku
              updated_at
              created_at
              image {
                filename
                size
              }
              price {
                min
                max
              }
              variant {
                key
                image
                price
                sku
                varStock
              }
              categories {
                name
                slug
              }
            }
          }
        }
        inquiry{
          id
          member {
            id
            name
            email
          }
          inquiries {
            id
            productImage
            productName
            description
            quantity
            price
            totalPrice
            status{
              statusType
              statusNote
            }
          }
          cart_id
          total_price_cart
          status
          IP
        }
        status
        total_price
        type
        created_at
      }
    }
`

export const FooterQuery = gql`
    query getFooter{
      footers{
        id
        left{
          name
          link
        }
        middle{
          icon
          value
        }
        address
        from
        to
        fromdays
        todays
        mitra{
          filename
          size
        }
      }
    }
`