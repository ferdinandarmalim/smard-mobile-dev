import gql from 'graphql-tag';

export const AddToCartMutation = gql`
    mutation addToCart($cartID: String! $memberID: String! $productID: String! $IP: String! $variant: String! $quantity: Int! $totalPriceProduct: Float! $totalPriceCart: Float! $cartType: String!){
        addToCart(cartID:$cartID memberID: $memberID productID: $productID IP: $IP variant: $variant quantity: $quantity totalPriceProduct: $totalPriceProduct totalPriceCart: $totalPriceCart cartType: $cartType){
            id
            member{
                id
                name
                email
            }
            products{
                productID
                productName
                variant
                quantity
                price
                totalPrice
                detailProduct{
                    id
                    name
                    weight{
                        unit
                        weight
                    }
                }
            }
            IP
            type
            total_price
            status
            created_at
            updated_at
        }
    }
`

export const RemoveFromCartMutation = gql`
    mutation removeFromCart($cartID: String! $productID: String! $variant: String! $totalPriceCart: Float!){
        removeFromCart(cartID: $cartID productID: $productID variant: $variant totalPriceCart: $totalPriceCart){
            id
            total_price
        }
    }
`

export const FinalizeOrderMutation = gql`
    mutation finalizeOrder($memberID: String! $cartID: String! $InquiryID: String $orderType: String!) {
        finalizeOrder(memberID: $memberID cartID: $cartID InquiryID: $InquiryID orderType: $orderType) {
            id
        }
    }  
`

export const RegisterMemberMutation = gql`
    mutation registerMember($name: String! $email: String! $phone: String $sosmed_status: Boolean $sosmed_register: String $regType: String! $uriTarget: String!){
        registerMember(name: $name email: $email phone: $phone sosmed_status: $sosmed_status sosmed_register: $sosmed_register regType: $regType uriTarget: $uriTarget){
            id
            name
            email
            phone
            status
            created_at
        }
    }
`

export const LoginMemberMutation = gql`
    mutation loginMember($email: String! $password: String!){
        loginMember(email: $email password: $password){
            id
            name
            phone
            email
            point
            dompet
            koin
        }
    }
`

export const VerificationMemberMutation = gql`
    mutation verificationB2C($verification: String! $password: String!) {
        verificationB2C(verification: $verification password: $password) {
            name
            email
        }
    }
`