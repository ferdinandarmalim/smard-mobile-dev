import React from 'react';
import { View } from 'react-native'
import { Text } from '@shoutem/ui';
import Ripple from 'react-native-material-ripple';

const HomeTopHeader = (props) => {
    return (
        <View style={styles.backgroundStyle}>
            <Ripple style={{height: '100%', flex: 0.5, borderRightWidth: 0.5, borderRightColor: '#D9D9D9', padding: 10}}>
                <Text style={styles.smallText}>Dompet</Text>
                <Text style={styles.valueStyle}>Rp. 120.000</Text>
            </Ripple>
            <Ripple style={{height: '100%', flex: 0.5, padding: 10}}>
                <Text style={styles.smallText}>Koin</Text>
                <Text style={styles.valueStyle}>Rp. 80.000</Text>
            </Ripple>
        </View>
    )
}

const styles = {
    backgroundStyle: {
        backgroundColor: '#FFF',
        height: 56,
        display: 'flex',
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: '#D9D9D9'
    },
    smallText:{
        fontSize: 12
    },
    valueStyle:{
        paddingTop: 5,
        fontFamily: 'Rubik-Medium'
    }
}

export default HomeTopHeader;