import React from 'react';
import { Card, Image, View, Subtitle, Caption } from '@shoutem/ui';
import config from '../../config';
import global from '../../global';

const ProductCard = (props) => {
    return (
        <Card>
            <View>
                <Image style={{ width: (global.deviceWidth/2-20), height: (global.deviceWidth/2-20), resizeMode: 'cover' }} source={{ uri: config.apiUrl + 'admin/img/products/' + props.data.image[0].filename }} />
                <Subtitle>{props.data.name}</Subtitle>
                <Caption>Rp {props.data.price[0].min}</Caption>
            </View>
        </Card>
    )
}

export default ProductCard;