import React from 'react';
import { Icon} from '@shoutem/ui';
import { createBottomTabNavigator } from 'react-navigation';

//Stack Navigator
import HomeStack from './pages/home/HomeStack';
import OrderStack from './pages/order/OrderStack';
import CartStack from './pages/cart/CartStack';
import OthersStack from './pages/others/OthersStack';
import FundsStack from './pages/funds/FundsStack';

const TabNavigatorAfterLogin = createBottomTabNavigator(
    {
        Home: {
            screen: HomeStack
        },
        Funds: {
            screen: FundsStack
        },
        Order: {
            screen: OrderStack
        },
        Cart: {
            screen: CartStack
        },
        Others: {
            screen: OthersStack
        }
    },
    {
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state
                let iconName;
                if (routeName === 'Home')
                    iconName = "home"
                else if (routeName === 'Promotion')
                    iconName = "deals"
                else if (routeName === 'Order')
                    iconName = "history"
                else if (routeName === 'Cart')
                    iconName = "cart"
                else if (routeName === 'Others')
                    iconName = "more-horizontal";

                return <Icon name={iconName} style={{ color: tintColor }} />
            }
        }),
        tabBarOptions: {
            activeTintColor: '#CB242C',
            inactiveTintColor: '#959595'
        },
        initialRouteName: 'Home',
    }
);

export default TabNavigatorAfterLogin;