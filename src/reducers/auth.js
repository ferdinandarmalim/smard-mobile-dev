import { SIGN_IN, SIGN_OUT } from '../actions/auth/auth';

const initialState = {
    isLogin: false //fetch session nantinya
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SIGN_IN:
            return { ...state, isLogin: true };
        case SIGN_OUT:
            return { ...state, isLogin: false };
        default:
            return state;
    }
}
