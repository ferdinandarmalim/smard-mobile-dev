export const dummy = [
  {
    "name": "ARCHIPELAGO MONEY MARKET FUND",
    "nav": "1070.89",
    "onemonth": "0.56",
    "sixmonth": "2.92",
    "oneyear": "5.47"
  },
  {
    "name": "ARCHIPELAGO BALANCE FUND",
    "nav": "1668.76",
    "onemonth": "2.13",
    "sixmonth": "3.94",
    "oneyear": "29.65"
  },
  {
    "name": "EMCO MANTAP",
    "nav": "5936.82",
    "onemonth": "-5.97",
    "sixmonth": "-6.95",
    "oneyear": "-6.69"
  },
  {
    "name": "EMCO BAROKAH SYARIAH",
    "nav": "1246.10",
    "onemonth": "0.43",
    "sixmonth": "2.37",
    "oneyear": "4.87"
  },
  {
    "name": "MNC DANA EKUITAS",
    "nav": "3985.61",
    "onemonth": "1.94",
    "sixmonth": "-1.79",
    "oneyear": "-4.81"
  },
  {
    "name": "MNC DANA SYARIAH EKUITAS",
    "nav": "1082.40",
    "onemonth": "2.08",
    "sixmonth": "2.72",
    "oneyear": "-3.20"
  },
  {
    "name": "MNC DANA SBN",
    "nav": "1093.87",
    "onemonth": "0.70",
    "sixmonth": "1.05",
    "oneyear": "-2.83"
  },
  {
    "name": "MNC DANA LANCAR",
    "nav": "1438.60",
    "onemonth": "0.54",
    "sixmonth": "2.52",
    "oneyear": "5.11"
  },
  {
    "name": "MNC DANA KOMBINASI ICON",
    "nav": "1355.79",
    "onemonth": "1.82",
    "sixmonth": "12.03",
    "oneyear": "2.85"
  },
  {
    "name": "FIRST STATE INDOEQUITY DIVIDEND YIELD FUND",
    "nav": "4779.87",
    "onemonth": "1.64",
    "sixmonth": "7.42",
    "oneyear": "-4.91"
  }
]