import React from 'react';
import { View, Text } from 'react-native';

const LoadingPage = (props) => {
    return (
        <View style={styles.viewStyle}>
            <Text>Loading...</Text>
        </View>
    )
}

const styles = {
    viewStyle: {
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    }
}

export default LoadingPage;