import React from 'react';
import { createStackNavigator } from 'react-navigation';
import HomePage from './HomePage';
import { Icon } from '@shoutem/ui';
import { style } from '../../style';

const HomeStack = createStackNavigator({
    Home: {
        screen: HomePage,
        navigationOptions: {
            title: 'SMARD',
            headerStyle: style.backgroundPrimary,
            headerTitleStyle: [style.bold, style.white]
        }
    }
})

export default HomeStack;
