import React from 'react';
import { Dimensions } from 'react-native';
import { Screen, Text, ScrollView, View, Title, TextInput, Button, Image } from '@shoutem/ui';
import { createStackNavigator } from 'react-navigation';
import GridLayout from 'react-native-layout-grid';
import { ProductsQuery } from '../../actions/graphql/queries';
import ProductCard from '../../components/products/ProductCard';
import { style } from '../../style';
import Ripple from 'react-native-material-ripple';

import store from '../../store';
import { connect } from 'react-redux';
import Carousel from 'react-native-carousel-view';

class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            initial: 0,
            interest: 0,
            period: 0,
            future: 0
        }
    }

    calculateSimulation() {
        this.setState({
            future: (this.state.initial + (this.state.initial * (this.state.interest / 100) * this.state.period))
        })
    }

    render() {
        return (
            <Screen>
                <ScrollView>
                    <Carousel delay={2000} hideIndicators={true} width={Dimensions.get('window').width}>
                        <View style={{width:Dimensions.get('window').width}}>
                            <Image styleName="large-banner" source={{ uri: 'https://ecs7.tokopedia.net/img/blog/promo/2018/05/JT-May-Promo-Thumbnail-600x328.jpg' }} />
                        </View>
                        <View style={{width:Dimensions.get('window').width}}>
                            <Image styleName="large-banner" source={{ uri: 'https://ecs7.tokopedia.net/img/blog/promo/2018/05/Thumbnail6.jpg' }} />
                        </View>
                        <View style={{width:Dimensions.get('window').width}}>
                            <Image styleName="large-banner" source={{ uri: 'https://ecs7.tokopedia.net/img/blog/promo/2018/05/thumbnail_600x328.png' }} />
                        </View>
                    </Carousel>
                    <View style={{ padding: 15, paddingBottom: 5 }}>
                        <Title>What is SMARD?</Title>
                        <Text>SMARD was established in 2015 with a vision to promote and provide a positive difference to investors in Indonesia.  We believe that investing should be easy and transparent, as this is the only way to facilitate investors in Indonesia to choose and make their investment decisions.</Text>
                        <Title style={{ marginTop: 10 }}>What is Mutual Funds?</Title>
                        <Text>A mutual fund is an investment vehicle made up of a pool of moneys collected from many investors for the purpose of investing in securities such as stocks, bonds, money market instruments and other assets.</Text>
                    </View>
                    <View style={{ padding: 15, paddingTop: 5 }}>
                        <Title style={{ marginBottom: 10 }}>Simulation</Title>
                        <View style={{ alignItems: 'center', padding: 20 }}>
                            <Text>Future Value: Rp. {this.state.future}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ paddingTop: 17, paddingLeft: 10, backgroundColor: 'white', marginBottom: 2 }}>Rp.</Text>
                            <TextInput keyboardType="numeric" style={{ marginBottom: 2, flex: 1 }} placeholder="Initial Investment Value" onChangeText={(val) => this.setState({ initial: parseFloat(val) })} />
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <TextInput keyboardType="numeric" style={{ marginBottom: 2, flex: 1 }} placeholder="Interest Rate" onChangeText={(val) => this.setState({ interest: parseFloat(val) })} />
                            <Text style={{ paddingTop: 17, paddingRight: 10, backgroundColor: 'white', marginBottom: 2 }}>%</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <TextInput keyboardType="numeric" style={{ marginBottom: 2, flex: 1 }} placeholder="Invesment Period" onChangeText={(val) => this.setState({ period: parseInt(val) })} />
                            <Text style={{ paddingTop: 17, paddingRight: 10, backgroundColor: 'white', marginBottom: 2 }}>Years</Text>
                        </View>

                        <Ripple onPress={() => this.calculateSimulation()}>
                            <Button style={style.backgroundAccent}>
                                <Text style={style.white}>CALCULATE</Text>
                            </Button>
                        </Ripple>
                    </View>
                    <View style={{ alignItems: 'center', margin: 20 }}>
                        <Text>SMARD is registered and supervised by OJK</Text>
                        <Image styleName="medium-wide" source={{ uri: 'https://www.supermarketreksadana.com/assets/img/logo_ojk2.png' }} />
                    </View>
                </ScrollView>
            </Screen>
        )
    }
}

const mapStateToProps = (state) => { return state };

export default connect(mapStateToProps)(HomePage);