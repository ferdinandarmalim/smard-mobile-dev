import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { Icon, TouchableOpacity } from '@shoutem/ui';
import { style } from '../../style';
import OthersPage from './OthersPage';
import ProfilePage from './profile/ProfilePage';

const OthersStack = createStackNavigator({
    Others: {
        screen: OthersPage,
        navigationOptions: {
            title: 'OTHERS',
            headerStyle: style.backgroundPrimary,
            headerTitleStyle: [style.bold, style.white]
        }
    },
    ProfilePage: {
        screen: ProfilePage,
        navigationOptions: {
            title: 'PROFILE',
            headerStyle: style.backgroundPrimary,
            headerTitleStyle: [style.bold, style.white],
            headerTintColor: '#FFFFFF'
        }
    }
})

export default OthersStack;
