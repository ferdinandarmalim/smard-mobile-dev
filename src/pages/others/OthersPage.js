import React from 'react';
import { Screen, Text, View, ListView, Row, Icon } from '@shoutem/ui';
import Ripple from 'react-native-material-ripple'
import { style } from '../../style';
//Redux
import { connect } from 'react-redux';
import store from '../../store';
import { bindActionCreators } from 'redux';
import * as authActions from '../../actions/auth/auth';

class OthersPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            otherRows: [
                { name: 'Profile', icon: 'user-profile', link: 'ProfilePage' },
                { name: 'Message', icon: 'comment', link: 'ProfilePage' },
                { name: 'Catalogue', icon: 'page', link: 'ProfilePage' },
                { name: 'Store Location', icon: 'maps', link: 'ProfilePage' },
                { name: 'Settings', icon: 'settings', link: 'ProfilePage' },
            ]
        }
    }

    logout() {
        store.dispatch({ type: 'SIGN_OUT' });
        this.props.navigation.navigate('Home');
    }

    gotoPage(page){
        this.props.navigation.navigate('ProfilePage');
    }

    render() {
        return (
            <Screen>
                {
                    this.state.otherRows.map((row) => {
                        return (
                            <Ripple key={row.name} onPress={()=>this.gotoPage(row.link)}>
                                <Row styleName="small">
                                    <Icon name={row.icon} />
                                    <Text>{row.name}</Text>
                                    <Icon styleName="disclosure" name="right-arrow" />
                                </Row>
                            </Ripple>
                        )
                    })
                }
                <Ripple onPress={() => this.logout()}>
                    <Row styleName="small">
                        <Icon name="exit-to-app" />
                        <Text>Logout</Text>
                        <Icon styleName="disclosure" name="right-arrow" />
                    </Row>
                </Ripple>
            </Screen>
        )
    }
}

const mapDispatchToProps = (dispatch) => { return bindActionCreators(authActions, dispatch) };

export default connect(mapDispatchToProps)(OthersPage);