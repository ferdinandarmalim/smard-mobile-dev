import React from 'react';
import { Screen, Text, Image, View, Icon, Button, Divider } from '@shoutem/ui';
import { style } from '../../../style';

class ProfilePage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Screen>
                <View styleName="vertical h-center" style={{ margin: 20 }}>
                    <Image
                        styleName="medium-avatar"
                        source={{ uri: 'http://www.ishootshows.com/wp-content/uploads/2008/07/todd-owyoung-portrait-145238_COB8628-square-600px.jpg' }}
                    />
                </View>
                <View style={{ margin: 20 }}>
                    <Text style={{ marginBottom: 10 }}>Faishal Amrullah</Text>
                    <View styleName="horizontal v-center" style={{ marginBottom: 10 }}>
                        <Icon style={{ marginRight: 10 }} name="email" />
                        <Text>faishal@fiture.id</Text>
                    </View>
                    <View styleName="horizontal v-center">
                        <Icon style={{ marginRight: 10 }} name="call" />
                        <Text>0812345678912</Text>
                    </View>
                </View>
                <View style={{ marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ marginBottom: 10 }}>Dompet</Text>
                    <Text style={{ marginBottom: 10 }}>Rp. 1.200.000</Text>
                    <Text style={{ marginBottom: 10 }}>Saldo</Text>
                    <Text style={{ marginBottom: 10 }}>Rp. 120.000</Text>
                    <Text style={{ marginBottom: 10 }}>Point</Text>
                    <Text style={{ marginBottom: 10 }}>8000</Text>
                </View>
                <View style={{position: 'absolute', bottom: 0, width: '100%'}}>
                    <Button style={style.backgroundAccent}><Text style={style.white}>EDIT PROFILE</Text></Button>
                </View>
            </Screen>
        )
    }
}

export default ProfilePage;
