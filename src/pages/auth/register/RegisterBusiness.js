import React from 'react';
import { View, TextInput, Button, Text } from '@shoutem/ui';
import { style } from '../../../style';

export default class RegisterBusiness extends React.Component {
    render() {
        return (
            <View style={{margin: 10}}>
                <TextInput placeholder={"Business Name"} />
                <TextInput placeholder={"Department Name"} />
                <TextInput placeholder={"Business Type"} />
                <TextInput placeholder={"Total Employee"} />
                <TextInput placeholder={"Full Name"} />
                <TextInput placeholder={"Phone Number"} />
                <TextInput placeholder={"Email"} />
                <Button style={style.backgroundAccent}>
                    <Text style={style.white}>REGISTER BUSINESS ACCOUNT</Text>
                </Button>
            </View>
        )
    }
}

const styles = {
    registerLink: {
        color: '#526FB3',
        fontSize: 12,
        marginTop: 12
    }
}