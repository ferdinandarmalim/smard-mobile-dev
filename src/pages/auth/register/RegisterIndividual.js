import React from 'react';
import { View, TextInput, Button, Text } from '@shoutem/ui';
import Ripple from 'react-native-material-ripple';

import { style } from '../../../style';

export default class RegisterIndividual extends React.Component {
    render() {
        return (
            <View>
                <TextInput placeholder={"Full Name"} />
                <TextInput placeholder={"Phone Number"} />
                <TextInput placeholder={"Email"} />
                <Ripple>
                    <Button style={style.backgroundAccent}>
                        <Text style={style.white}>REGISTER</Text>
                    </Button>
                </Ripple>
            </View>
        )
    }
}

const styles = {
    registerLink: {
        color: '#526FB3',
        fontSize: 12,
        marginTop: 12
    }
}