import React from 'react';
import { Screen, View, Text } from '@shoutem/ui';
import Ripple from 'react-native-material-ripple';
import RegisterIndividual from './register/RegisterIndividual';
import LoginIndividual from './login/LoginIndividual';

class AuthPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isIndividual: true
        }
    }

    toggleButton(type) {
        this.setState({
            isIndividual: type === 'b2c' ? true : false
        })
    }

    render() {
        return (
            <Screen>
                <View style={styles.tabStyle}>
                    <Ripple style={{ height: '100%', flex: 0.5, borderRightWidth: 0.5, borderRightColor: '#D9D9D9', justifyContent: 'center', alignItems: 'center' }} onPress={() => this.toggleButton('b2c')}>
                        <Text>LOGIN</Text>
                    </Ripple>
                    <Ripple style={{ height: '100%', flex: 0.5, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.toggleButton('b2b')}>
                        <Text>REGISTER</Text>
                    </Ripple>
                </View>
                <View style={{ margin: 10 }}>
                    {this.state.isIndividual ? <LoginIndividual /> : <RegisterIndividual />}
                </View>
            </Screen>
        )
    }
}

const styles = {
    tabStyle: {
        backgroundColor: '#FFF',
        height: 56,
        display: 'flex',
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: '#D9D9D9'
    }
}

export default AuthPage;