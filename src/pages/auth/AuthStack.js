import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { style } from '../../style';
import AuthPage from './AuthPage';
import RegisterBusiness from './register/RegisterBusiness';
import RegisterIndividual from './register/RegisterIndividual';

const AuthStack = createStackNavigator({
    Auth: {
        screen: AuthPage,
        navigationOptions: {
            title: 'LOGIN',
            headerStyle: style.backgroundPrimary,
            headerTitleStyle: [style.bold, style.white],
            headerTintColor: '#FFFFFF'
        }
    },
    RegisterBusiness: {
        screen: RegisterBusiness,
        navigationOptions: {
            title: 'REGISTER BUSINESS',
            headerStyle: style.backgroundPrimary,
            headerTitleStyle: [style.bold, style.white],
            headerTintColor: '#FFFFFF'
        }
    },
    RegisterIndividual: {
        screen: RegisterIndividual,
        navigationOptions: {
            title: 'REGISTER INDIVIDUAL',
            headerStyle: style.backgroundPrimary,
            headerTitleStyle: [style.bold, style.white],
            headerTintColor: '#FFFFFF'
        }
    }
})

export default AuthStack;
