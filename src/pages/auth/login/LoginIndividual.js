import React from 'react';
import { View, TextInput, Button, Text, TouchableOpacity } from '@shoutem/ui';
import Ripple from 'react-native-material-ripple';
import { connect } from 'react-redux';
import { style } from '../../../style';
import global from '../../../global';
import CartPage from '../../cart/CartPage';
//Redux=
import store from '../../../store';
import { bindActionCreators } from 'redux';
import * as authActions from '../../../actions/auth/auth';

class LoginIndividual extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            error: ''
        }
    }

    submit() {
        if (this.state.email != this.state.password) {
            this.setState({ error: 'email and password is not the same' })
        } else {
            this.setState({ error: 'ok' })
        }
        console.log(this.state)
    }

    render() {
        return (
            <View>
                <TextInput placeholder={"Email"} onChangeText={(value) => this.setState({ email: value })} />
                <TextInput placeholder={"Password"} secureTextEntry onChangeText={(value) => this.setState({ password: value })} />
                <Ripple onPress={() => this.submit()}>
                    <Button style={style.backgroundAccent}>
                        <Text style={style.white}>LOGIN INDIVIDUAL</Text>
                    </Button>
                </Ripple>
            </View>
        )
    }
}

const styles = {
    registerLink: {
        color: '#526FB3',
        fontSize: 12,
        marginTop: 12
    }
}

const mapDispatchToProps = (dispatch) => { return bindActionCreators(authActions, dispatch) };

export default connect(mapDispatchToProps)(LoginIndividual);
