import React from 'react';
import { View, TextInput, Button, Text, TouchableOpacity } from '@shoutem/ui';
import { style } from '../../../style';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as authActions from '../../../actions/auth/auth';

class LoginBusiness extends React.Component {
    render() {
        return (
            <View>
                <TextInput placeholder={"Business Email"} />
                <TextInput placeholder={"Password"} secureTextEntry />
                <Button style={style.backgroundAccent}>
                    <Text style={style.white}>LOGIN AS BUSINESS ACCOUNT</Text>
                </Button>
                <TouchableOpacity onPress={()=>this.props.nav.push('RegisterBusiness')}>
                    <Text style={styles.registerLink}>New here? Register instead</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = {
    registerLink: {
        color: '#526FB3',
        fontSize: 12,
        marginTop: 12
    }
}

const mapDispatchToProps = (dispatch) => { return bindActionCreators(authActions, dispatch) };

export default connect(mapDispatchToProps)(LoginBusiness);