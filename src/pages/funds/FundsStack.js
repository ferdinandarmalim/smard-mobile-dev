import { createStackNavigator } from 'react-navigation';
import FundsPage from './FundsPage';
import { style } from '../../style';

const FundsStack = createStackNavigator({
    Funds: {
        screen: FundsPage,
        navigationOptions: {
            title: 'MUTUAL FUNDS',
            headerStyle: style.backgroundPrimary,
            headerTitleStyle: [style.bold, style.white]
        }
    }
})

export default FundsStack;