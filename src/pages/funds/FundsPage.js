import React from 'react';
import { ScrollView, Screen, Text, TextInput, View, Button, LoadingIndicator } from '@shoutem/ui';
import Ripple from 'react-native-material-ripple';
import {dummy} from "../../assets/dummy";

const promotionDummy = [
    { name: 'PROMO RAMADHAN SCHNEIDER', date: '1 Aug - 1 Des', img: 'https://ecs7.tokopedia.net/img/blog/promo/2018/05/JT-May-Promo-Thumbnail-600x328.jpg' },
    { name: 'PROMO PHILLIPS MURAH MERIAH', date: '18 Aug - 18 Sep', img: 'https://ecs7.tokopedia.net/img/blog/promo/2018/05/thumbnail11.png' },
    { name: 'PROMO MCB TERBAIK', date: '20 Aug - 20 Sep', img: 'https://ecs7.tokopedia.net/img/blog/promo/2018/05/thumbnail-72.png' },
]


const apiURL = 'http://103.82.241.18/smard/Listproduct/products'

class FundsPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            searchQuery: '',
            isLoading: true,
            fundsData: dummy,
            filteredSearch: dummy
        }

        console.log(this.state)
    }

    componentDidMount() {
        return fetch(apiURL)
            .then((response) => {
                response.json().then((data) => {
                    this.setState({
                        isLoading: false,
                        fundsData: data.aaData,
                        filteredSearch: data.aaData
                    })
                });
            })
            .catch((error) => {
                console.error(error);
            });
    }

    filterSearch(search) {
        this.setState({
            searchQuery: search
        });
        if (this.state.searchQuery == '') {
            this.setState({
                filteredSearch: this.state.fundsData
            })
        }
        else {
            this.setState({
                filteredSearch: this.state.fundsData.filter(data => {
                    return data.name.toLowerCase().indexOf(search.toLowerCase()) > -1
                })
            });
        }
    }

    render() {
        return (
            <Screen style={{ backgroundColor: '#e8e8e8' }}>
                <TextInput style={{ marginBottom: 5 }} placeholder="Search..." onChangeText={(search) => this.filterSearch(search)} />
                <ScrollView>
                    {/* {this.state.isLoading ?
                        <LoadingIndicator /> :
                        this.state.filteredSearch.map((data) => {
                            return (
                                <View style={{ marginLeft: 15, marginRight: 15, marginBottom: 5, marginTop: 5, backgroundColor: 'white', padding: 10 }} key={data.name}>
                                    <View style={{ borderBottomColor: '#D9D9D9', borderBottomWidth: 1, paddingBottom: 10 }}>
                                        <Text style={{ fontFamily: 'Rubik-Medium' }}>Product Name</Text>
                                        <Text>{data.name}</Text>
                                    </View>
                                    <View style={{ marginTop: 10, marginBottom: 10, flexDirection: 'row', justifyContent: 'space-between', }}>
                                        <View>
                                            <Text style={{ fontFamily: 'Rubik-Medium' }}>Last Nav</Text>
                                            <Text>{data.last_nab}</Text>
                                        </View>
                                        <View>
                                            <Text style={{ fontFamily: 'Rubik-Medium' }}>1 mo.</Text>
                                            <Text>{data.return_1_mth}</Text>
                                        </View>
                                        <View>
                                            <Text style={{ fontFamily: 'Rubik-Medium' }}>6 mos.</Text>
                                            <Text>{data.return_6_mth}</Text>
                                        </View>
                                        <View>
                                            <Text style={{ fontFamily: 'Rubik-Medium' }}>1 yr.</Text>
                                            <Text>{data.return_1_yrs}</Text>
                                        </View>
                                    </View>
                                    <Ripple onPress={() => this.props.navigation.navigate('Login')}>
                                        <Button style={{ backgroundColor: '#67a358' }}>
                                            <Text style={{ color: 'white' }}>BUY</Text>
                                        </Button>
                                    </Ripple>
                                </View>

                            )
                        })} */}
                    {this.state.filteredSearch.map((data) => {
                        return (
                            <View style={{ marginLeft: 15, marginRight: 15, marginBottom: 5, marginTop: 5, backgroundColor: 'white', padding: 10 }} key={data.name}>
                                <View style={{ borderBottomColor: '#D9D9D9', borderBottomWidth: 1, paddingBottom: 10 }}>
                                    <Text style={{ fontFamily: 'Rubik-Medium' }}>Product Name</Text>
                                    <Text>{data.name}</Text>
                                </View>
                                <View style={{ marginTop: 10, marginBottom: 10, flexDirection: 'row', justifyContent: 'space-between', }}>
                                    <View>
                                        <Text style={{ fontFamily: 'Rubik-Medium' }}>Last Nav</Text>
                                        <Text>{data.nav}</Text>
                                    </View>
                                    <View>
                                        <Text style={{ fontFamily: 'Rubik-Medium' }}>1 mo.</Text>
                                        <Text>{data.onemonth}</Text>
                                    </View>
                                    <View>
                                        <Text style={{ fontFamily: 'Rubik-Medium' }}>6 mos.</Text>
                                        <Text>{data.sixmonth}</Text>
                                    </View>
                                    <View>
                                        <Text style={{ fontFamily: 'Rubik-Medium' }}>1 yr.</Text>
                                        <Text>{data.oneyear}</Text>
                                    </View>
                                </View>
                                <Ripple onPress={() => this.props.navigation.navigate('Login')}>
                                    <Button style={{ backgroundColor: '#67a358' }}>
                                        <Text style={{ color: 'white' }}>BUY</Text>
                                    </Button>
                                </Ripple>
                            </View>
                        )
                    })}
                </ScrollView>
            </Screen>

        )
    }
}

export default FundsPage;