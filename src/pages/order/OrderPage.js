import React from 'react';
import { Screen, ScrollView, Text, Button, View, Tile, Image, Caption, Title } from '@shoutem/ui';
import Ripple from 'react-native-material-ripple'
import { style } from '../../style';
import OngoingOrder from './ongoing/OngoingOrder';
import OrderHistory from './history/OrderHistory';

class OrderPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            news: [
                { name: 'IHSG Bisa Kembali Lanjutkan Perjalanan di Zona Hijau', date: '2 hours ago', img: 'https://www.supermarketreksadana.com/assets/news/4dfbed3c49b04901d0ae932719ce3bdf.jpg', content:'JawaPos.com - Pergerakan indeks harga saham gabungan yang kemarin sukses ditutup positif dapat kembali melanjutkan perjalanannya di jalur hijau. Namun, kenaikan yang terjadi kembali harus diuji ketahanannya seiring posisinya yang masih dalam tren penurunan. Menurut analis PT Binaartha Sekuritas Reza Priyambada mengatakan, kemungkinan berbalik melemah pun juga terbuka jika sentimen yang ada, terutama dari pergerakan sejumlah indeks saham global, tidak bergerak dalam zona positif. ”Diperkirakan IHSG akan berada di kisaran support 5.723-5.736 dan resisten 5.767-5.775. Tetap mewaspadai terhadap sentimen-sentimen yang dapat membuat IHSG kembali melemah,” ujarnya di Jakarta, Rabu (23/5). Reza menjelaskan, menguatnya laju bursa saham Amerika Serikat (AS) memberikan sentimen positif pada pergerakan IHSG dimana mampu berbalik positif. “Pelaku pasar memanfaatkan pelemahan sebelumnya untuk kembali masuk,” ucapnya. Terlihat pergerakan IHSG bertahan di zona hijau hingga akhir perdagangan meski kembali turun setelah menyentuh level tertingginya. Menurutnya, kembali terapresiasinya Rupiah juga cukup membantu IHSG untuk kembali ke zona hijau meski juga dibarengi dengan adanya aksi jual asing yang menahan pergerakan IHSG untuk bergerak lebih tinggi lagi.' },
                { name: 'Rupiah masih membayangi penurunan IHSG', date: '4 days ago', img: 'https://www.supermarketreksadana.com/assets/news/7850e57e64dfc31831a13eda6afe5f50.jpg', content: 'KONTAN.CO.ID - JAKARTA. Dalam perdagangan hari ini, Indeks Harga Saham Gabungan (IHSG) sempat terjerembab ke level 1% meski akhirnya IHSG mempersempit penurunan menjadi 0,56%. Penurunan indeks ini mengikuti koreksi di hari sebelumnya. Muhammad Nafan Aji, Analis Binaartha Parama Sekuritas menilai bahwa penurunan yang terjadi pada indeks hari ini sebenarnya berbanding terbalik dengan mayoritas indeks yang ada di Asia. "Penurunan indeks terutama disebabkan minimnya sentimen dalam negeri," kata Nafan, Senin (21/5). Nafan mengatakan beberapa putusan pemerintah tengah ditunggu oleh investor seperti penerapan tax holiday. Selain itu, makin melemahnya rupiah pada pagi ini juga jadi salah satu faktor yang membuat indeks terperosok. Namun demikian, Nafan memprediksi bahwa rupiah akan stabil atau bahkan rebound. Menurut Nafan, Indeks hari ini akan bergerak di level support di angka 5.716 sementara level ressistance indeks berada di level 5.790. Di pasar spot pukul 10.03 WIB pagi ini, nilai tukar rupiah berada di Rp 14.178 per dollar AS. Rupiah melemah 0,15% daripada akhir pekan lalu di Rp 14.156 per dollar AS.' },
                { name: 'BEI: IHSG Anjlok Bukan karena Aksi Teror Bom Surabaya', date: '2 May 2018', img: 'https://www.supermarketreksadana.com/assets/news/78800c8514a6dfb859c11607b5264e1a.jpg', content:'JawaPos.com - Pergerakan indeks harga saham gabungan yang kemarin sukses ditutup positif dapat kembali melanjutkan perjalanannya di jalur hijau. Namun, kenaikan yang terjadi kembali harus diuji ketahanannya seiring posisinya yang masih dalam tren penurunan. Menurut analis PT Binaartha Sekuritas Reza Priyambada mengatakan, kemungkinan berbalik melemah pun juga terbuka jika sentimen yang ada, terutama dari pergerakan sejumlah indeks saham global, tidak bergerak dalam zona positif. ”Diperkirakan IHSG akan berada di kisaran support 5.723-5.736 dan resisten 5.767-5.775. Tetap mewaspadai terhadap sentimen-sentimen yang dapat membuat IHSG kembali melemah,” ujarnya di Jakarta, Rabu (23/5). Reza menjelaskan, menguatnya laju bursa saham Amerika Serikat (AS) memberikan sentimen positif pada pergerakan IHSG dimana mampu berbalik positif. “Pelaku pasar memanfaatkan pelemahan sebelumnya untuk kembali masuk,” ucapnya. Terlihat pergerakan IHSG bertahan di zona hijau hingga akhir perdagangan meski kembali turun setelah menyentuh level tertingginya. Menurutnya, kembali terapresiasinya Rupiah juga cukup membantu IHSG untuk kembali ke zona hijau meski juga dibarengi dengan adanya aksi jual asing yang menahan pergerakan IHSG untuk bergerak lebih tinggi lagi.' },
                { name: 'Harga Emas Turun karena Tekanan Dolar AS', date: '28 April 2018', img: 'https://www.supermarketreksadana.com/assets/news/4dfbed3c49b04901d0ae932719ce3bdf.jpg', content: 'KONTAN.CO.ID - JAKARTA. Dalam perdagangan hari ini, Indeks Harga Saham Gabungan (IHSG) sempat terjerembab ke level 1% meski akhirnya IHSG mempersempit penurunan menjadi 0,56%. Penurunan indeks ini mengikuti koreksi di hari sebelumnya. Muhammad Nafan Aji, Analis Binaartha Parama Sekuritas menilai bahwa penurunan yang terjadi pada indeks hari ini sebenarnya berbanding terbalik dengan mayoritas indeks yang ada di Asia. "Penurunan indeks terutama disebabkan minimnya sentimen dalam negeri," kata Nafan, Senin (21/5). Nafan mengatakan beberapa putusan pemerintah tengah ditunggu oleh investor seperti penerapan tax holiday. Selain itu, makin melemahnya rupiah pada pagi ini juga jadi salah satu faktor yang membuat indeks terperosok. Namun demikian, Nafan memprediksi bahwa rupiah akan stabil atau bahkan rebound. Menurut Nafan, Indeks hari ini akan bergerak di level support di angka 5.716 sementara level ressistance indeks berada di level 5.790. Di pasar spot pukul 10.03 WIB pagi ini, nilai tukar rupiah berada di Rp 14.178 per dollar AS. Rupiah melemah 0,15% daripada akhir pekan lalu di Rp 14.156 per dollar AS.'  },
            ]
        }
    }

    gotoDetail(data) {
        this.props.navigation.navigate('Detail', { data })
    }

    render() {
        return (
            <Screen>
                <ScrollView>
                    <View style={{ marginLeft: 5, marginTop: 10, marginRight: 5 }}>
                        {this.state.news.map((data) => {
                            return (
                                <Ripple key={data.name} onPress={() => this.gotoDetail(data)}>
                                    <Tile style={{ marginBottom: 10 }}>
                                        <Image
                                            styleName="large-banner"
                                            source={{ uri: data.img }}
                                        />
                                        <View styleName="content">
                                            <Title>{data.name}</Title>
                                            <View>
                                                <Caption>{data.date}</Caption>
                                            </View>
                                        </View>
                                    </Tile>
                                </Ripple>
                            )
                        })}
                    </View>
                </ScrollView>
            </Screen>
        )
    }
}
export default OrderPage;