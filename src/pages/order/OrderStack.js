import { createStackNavigator } from 'react-navigation';
import OrderPage from './OrderPage';
import OrderHistory from './history/OrderHistory';
import { style } from '../../style';

const OrderStack = createStackNavigator({
    Order: {
        screen: OrderPage,
        navigationOptions: {
            title: 'NEWS',
            headerStyle: style.backgroundPrimary,
            headerTitleStyle: [style.bold, style.white]
        }
    },
    Detail: {
        screen: OrderHistory,
        navigationOptions: {
            title: 'NEWS',
            headerStyle: style.backgroundPrimary,
            headerTitleStyle: [style.bold, style.white],
            headerTintColor: '#FFF'
        }
    }
})

export default OrderStack;