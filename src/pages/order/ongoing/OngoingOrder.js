import React from 'react';
import { View, ScrollView, Text, Icon, Button, Subtitle } from '@shoutem/ui';
import { style } from '../../../style';

class OngoingOrder extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isDetail: false
        }
    }

    toggleDetail() {
        this.setState({ isDetail: !this.state.isDetail })
    }

    render() {
        return (
            <View style={{paddingBottom: 56, paddingLeft: 10, paddingTop: 10, paddingRight: 10}}>
                <ScrollView>
                    <View style={{ backgroundColor: '#FFF', paddingLeft: 10, paddingRight: 10, paddingTop: 10, marginBottom: 10 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flexDirection: 'column', marginRight: 20 }}>
                                <Text>Invoice Number</Text>
                                <Text>Transaction Date</Text>
                                <Text>Total Products</Text>
                                <Text>Total Price</Text>
                            </View>
                            <View style={{ flexDirection: 'column' }}>
                                <Text>INV 123</Text>
                                <Text>Aug 23 - Aug 28, 2018</Text>
                                <Text>2</Text>
                                <Text>Rp. 12.000.000</Text>
                            </View>
                        </View>
                        {
                            this.state.isDetail ?
                                <View>
                                    <Button styleName="full-width" onPress={() => this.toggleDetail()}>
                                        <Text style={style.colorAccent}>Hide Detail</Text>
                                        <Icon style={style.colorAccent} name="up-arrow" />
                                    </Button>
                                    <View style={{ borderWidth: 0.5, borderColor: '#D9D9D9', borderRadius: 5, padding: 10, marginBottom: 5 }}>
                                        <Subtitle>Cart</Subtitle>
                                        <View style={{ flexDirection: 'row', marginTop: 10, borderWidth: 0.5, borderColor: '#D9D9D9', borderRadius: 5, padding: 10 }}>
                                            <View style={{ flexDirection: 'column', marginRight: 20 }}>
                                                <Text>Product Name</Text>
                                                <Text>Product Variant</Text>
                                                <Text>Quantity</Text>
                                                <Text>Price</Text>
                                                <Text>Total Price</Text>
                                            </View>
                                            <View style={{ flexDirection: 'column' }}>
                                                <Text>SCHNEIDER</Text>
                                                <Text>Hitam</Text>
                                                <Text>10</Text>
                                                <Text>Rp. 1.200.000</Text>
                                                <Text>Rp. 12.000.000</Text>
                                            </View>
                                        </View>
                                        <View style={{ flexDirection: 'row', marginTop: 10, borderWidth: 0.5, borderColor: '#D9D9D9', borderRadius: 5, padding: 10 }}>
                                            <View style={{ flexDirection: 'column', marginRight: 20 }}>
                                                <Text>Product Name</Text>
                                                <Text>Product Variant</Text>
                                                <Text>Quantity</Text>
                                                <Text>Price</Text>
                                                <Text>Total Price</Text>
                                            </View>
                                            <View style={{ flexDirection: 'column' }}>
                                                <Text>SCHNEIDER</Text>
                                                <Text>Hitam</Text>
                                                <Text>10</Text>
                                                <Text>Rp. 1.200.000</Text>
                                                <Text>Rp. 12.000.000</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ borderWidth: 0.5, borderColor: '#D9D9D9', borderRadius: 5, padding: 10, marginBottom: 5 }}>
                                        <Subtitle>Inquiry</Subtitle>
                                        <View style={{ flexDirection: 'row', marginTop: 10, borderWidth: 0.5, borderColor: '#D9D9D9', borderRadius: 5, padding: 10 }}>
                                            <View style={{ flexDirection: 'column', marginRight: 20 }}>
                                                <Text>Product Name</Text>
                                                <Text>Product Variant</Text>
                                                <Text>Quantity</Text>
                                                <Text>Price</Text>
                                                <Text>Total Price</Text>
                                            </View>
                                            <View style={{ flexDirection: 'column' }}>
                                                <Text>SCHNEIDER</Text>
                                                <Text>Hitam</Text>
                                                <Text>10</Text>
                                                <Text>Rp. 1.200.000</Text>
                                                <Text>Rp. 12.000.000</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                                :
                                <Button styleName="full-width" onPress={() => this.toggleDetail()}>
                                    <Text style={style.colorAccent}>View Detail</Text>
                                    <Icon style={style.colorAccent} name="down-arrow" />
                                </Button>
                        }
                    </View>
                </ScrollView>
            </View>
        )
    }
}

export default OngoingOrder;