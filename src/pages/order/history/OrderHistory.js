import React from 'react';
import { Screen, View, ScrollView, Text, Icon, Button, Title, Image } from '@shoutem/ui';
import { style } from '../../../style';

class OrderHistory extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Screen>
                <ScrollView>
                    <Image styleName="large-banner" source={{ uri: this.props.navigation.state.params.data.img }} />
                    <View style={{padding: 15}}>
                        <Title style={{marginBottom: 10}}>{this.props.navigation.state.params.data.name}</Title>
                        <Text>{this.props.navigation.state.params.data.content}</Text>
                    </View>
                </ScrollView>
            </Screen>
        )
    }
}

export default OrderHistory;