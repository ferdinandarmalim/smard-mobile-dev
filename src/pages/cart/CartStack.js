import { createStackNavigator } from 'react-navigation';
import CartPage from './CartPage';
import { style } from '../../style';

const CartStack = createStackNavigator({
    Cart: {
        screen: CartPage,
        navigationOptions: {
            title: 'LEADERBOARD',
            headerStyle: style.backgroundPrimary,
            headerTitleStyle: [style.bold, style.white]
        }
    }
})

export default CartStack;