import React from 'react';
import { ScrollView, Screen, Title, Text, View, Image, Subtitle, Button } from '@shoutem/ui';
import Ripple from 'react-native-material-ripple';
import { style } from '../../style';
import global from '../../global';
//redux
import { connect } from 'react-redux';

class CartPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [
                { name: 'Budi Utomo', total: 232, profit: '800.000.000' },
                { name: 'Soekarno Hatta', total: 189, profit: '760.000.000' },
                { name: 'Zainal Arifin', total: 170, profit: '640.000.000' },
                { name: 'Andi Lukman', total: 167, profit: '520.000.000' },
                { name: 'Jusman Sinaga', total: 165, profit: '510.000.000' },
            ],
        }
    }

    render() {
        return (
            <Screen>
                <ScrollView>
                    <Title style={{padding: 15}}>Top User</Title>
                    {this.state.users.map((data) => {
                        return (
                            <View style={{ marginLeft: 15, marginRight: 15, marginBottom: 5, marginTop: 5, backgroundColor: 'white', padding: 10 }} key={data.name}>
                                <View style={{ borderBottomColor: '#D9D9D9', borderBottomWidth: 1, paddingBottom: 10 }}>
                                    <Text style={{ fontFamily: 'Rubik-Medium' }}>Name</Text>
                                    <Text>{data.name}</Text>
                                </View>
                                <View style={{ marginTop: 10, marginBottom: 10, flexDirection: 'row' }}>
                                    <View style={{flex:0.5}}>
                                        <Text style={{ fontFamily: 'Rubik-Medium' }}>Total Funds</Text>
                                        <Text>{data.total}</Text>
                                    </View>
                                    <View style={{flex:0.5}}>
                                        <Text style={{ fontFamily: 'Rubik-Medium' }}>Profit</Text>
                                        <Text>Rp. {data.profit}</Text>
                                    </View>
                                </View>
                            </View>
                        )
                    })}
                </ScrollView>
            </Screen>
        )
    }
}

const mapStateToProps = (state) => { return state }

export default connect(mapStateToProps)(CartPage);