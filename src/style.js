//Apply global style here

export const style = {
    bold: {
        fontFamily: 'Rubik-Medium',
    },
    backgroundPrimary: {
        backgroundColor: '#272737'
    },
    backgroundAccent: {
        backgroundColor: '#E58746'
    },
    colorPrimary: {
        color: '#272737'
    },
    colorAccent: {
        color: '#E58746'
    },
    white: {
        color: '#FFFFFF'
    },
    smallText: {
        fontSize: 10
    }
}