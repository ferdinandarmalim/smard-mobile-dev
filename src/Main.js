import React from 'react';
import { connect } from 'react-redux';
import TabNavigatorBeforeLogin from './TabNavigatorBeforeLogin';
import TabNavigatorAfterLogin from './TabNavigatorAfterLogin';

class Main extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            this.props.auth.isLogin ? <TabNavigatorAfterLogin /> : <TabNavigatorBeforeLogin />
        )
    }
}

const mapStateToProps = (state) => { return state; }

export default connect(mapStateToProps)(Main);