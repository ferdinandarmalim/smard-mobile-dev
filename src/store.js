import { createStore, combineReducers, applyMiddleware } from 'redux';
import auth from './reducers/auth';
import thunk from 'redux-thunk';

const reducer = combineReducers({ auth })

const store = createStore(
    reducer,
    applyMiddleware(thunk)
)

export default store;

