import React from 'react';
import { Platform } from 'react-native';
import { Provider } from 'react-redux';
import { Screen, Icon } from '@shoutem/ui';
import { Font } from 'expo';
import { ApolloClient, HttpLink, InMemoryCache } from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

import store from './src/store';

//Component +  Page
import Main from './src/Main';
import LoadingPage from './src/pages/loading/LoadingPage';

import { style } from './src/style';

const client = new ApolloClient({
  link: new HttpLink({ uri: 'http://103.82.241.18:4000/graphql' }),
  cache: new InMemoryCache()
});

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fontLoaded: false
    }
  }

  async componentWillMount() {
    try {
      await Font.loadAsync({
        'Rubik-Black': require('./node_modules/@shoutem/ui/fonts/Rubik-Black.ttf'),
        'Rubik-BlackItalic': require('./node_modules/@shoutem/ui/fonts/Rubik-BlackItalic.ttf'),
        'Rubik-Bold': require('./node_modules/@shoutem/ui/fonts/Rubik-Bold.ttf'),
        'Rubik-BoldItalic': require('./node_modules/@shoutem/ui/fonts/Rubik-BoldItalic.ttf'),
        'Rubik-Italic': require('./node_modules/@shoutem/ui/fonts/Rubik-Italic.ttf'),
        'Rubik-Light': require('./node_modules/@shoutem/ui/fonts/Rubik-Light.ttf'),
        'Rubik-LightItalic': require('./node_modules/@shoutem/ui/fonts/Rubik-LightItalic.ttf'),
        'Rubik-Medium': require('./node_modules/@shoutem/ui/fonts/Rubik-Medium.ttf'),
        'Rubik-MediumItalic': require('./node_modules/@shoutem/ui/fonts/Rubik-MediumItalic.ttf'),
        'Rubik-Regular': require('./node_modules/@shoutem/ui/fonts/Rubik-Regular.ttf'),
        'rubicon-icon-font': require('./node_modules/@shoutem/ui/fonts/rubicon-icon-font.ttf'),
      });
      this.setState({ fontLoaded: true });
    } catch (error) {
      this.setState({ fontLoaded: false });
    }
  }

  render() {
    return (
      <ApolloProvider client={client}>
        <Provider store={store}>
          {this.state.fontLoaded ? <Main /> : <LoadingPage />}
        </Provider>
      </ApolloProvider>
    )
  }
}

export default App;